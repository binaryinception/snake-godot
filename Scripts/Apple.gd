extends Sprite

var timer = Timer.new()
var rotSpeed = deg2rad(15)
var posSpeed = 50

func _ready():
	self.set_pos(Vector2(self.get_parent().get_rect().size.width / 2, self.get_parent().get_rect().size.height / 2))
	self.set_process(true)
	self.set_process_input(true)
	
	timer.connect("timeout", self, "tick")
	timer.set_wait_time(0.5)
	
	add_child(timer)
	#timer.start()

func tick():
	var rot = self.get_rot()
	rot += rotSpeed
	self.set_rot(rot)

func _input(event):
	var moveSpeed = 0
	
	if (Input.is_action_pressed("MoveRight")):
		moveSpeed += posSpeed
	
	if (Input.is_action_pressed("MoveLeft")):
		moveSpeed -= posSpeed
	
	self.set_pos(Vector2(self.get_pos().x + (moveSpeed), self.get_pos().y))
	
func _process(delta):
	self.set_rot(self.get_rot() + (rotSpeed * delta))
	
	var moveSpeed = 0
	
	if (Input.is_key_pressed(KEY_RIGHT)):
		moveSpeed += posSpeed
		
	if (Input.is_key_pressed(KEY_LEFT)):
		moveSpeed -= posSpeed
		
	if (Input.is_key_pressed(KEY_ESCAPE)):
		self.get_tree().quit()
		
	self.set_pos(Vector2(self.get_pos().x + (moveSpeed * delta), self.get_pos().y))
	
func _draw():
	self.draw_rect(self.get_item_rect(), Color(0, 0, 1, 0.2))
