extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
#onready var collisionShape = get_node("CollisionShape2D");
onready var tileMap = get_tree().get_root().get_node("Node2D/TileMap");
onready var tweenNode = get_node("Tween");
onready var collisionShape = RectangleShape2D.new();
var SnakeBody = preload("res://SnakeBody.tscn");

var tail = [];
enum Directions { NORTH, EAST, SOUTH, WEST };
var currentDirection = Directions.SOUTH;

func _ready():
	tweenNode.connect("tween_complete", self, "onMoved");
	
	collisionShape.set_extents(get_texture().get_size() / 3);
	
	add_user_signal("moved");
	add_user_signal("collision");

func onMoved(obj, key):
	emit_signal("moved");
	
	for t in tail:
		if (collisionShape.collide(get_transform(), t.collisionShape, t.get_transform())):
			print("Colliding with tail");
			emit_signal("collision");

func nextTurn():
	var relPos = Vector2();
	
	if (currentDirection == Directions.SOUTH):
		relPos.y += 1;
	
	if (currentDirection == Directions.NORTH):
		relPos.y -= 1;
	
	if (currentDirection == Directions.EAST):
		relPos.x += 1;
	
	if (currentDirection == Directions.WEST):
		relPos.x -= 1;
	
	var curPos = getCellPos();
	var nextPos = curPos + relPos;
	
	moveToCell(nextPos);
	
	for i in range(tail.size()):
		if (i == 0): tail[i].moveToCell(getCellPos());
		else: tail[i].moveToCell(tail[i-1].getCellPos());

func getCellPos():
	return tileMap.world_to_map(get_pos());

func moveToCell(pos):
	if (tileMap.isTileBlocked(pos)):
		emit_signal("collision");
		return;
	
	tweenNode.interpolate_property(self, "transform/pos", get_pos(), tileMap.map_to_world(pos), 0.5, Tween.TRANS_BACK, Tween.EASE_OUT);
	tweenNode.start();

func addBody():
	var newBody = SnakeBody.instance();
	var pos = get_pos();
	
	if (tail.size() > 0):
		var last = tail[tail.size() - 1];
		pos = last.get_pos();
	
	newBody.set_pos(pos);
	tail.append(newBody);
	get_parent().add_child(newBody);
	
func _exit_tree():
	for t in tail:
		get_parent().remove_child(t);
	