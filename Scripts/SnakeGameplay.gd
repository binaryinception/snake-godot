extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var AppleScene = preload("res://Apple.tscn");
onready var tileMap =  get_node("TileMap");
onready var snake = get_node("SnakeHead");
onready var turnTimer = Timer.new();
var currentApple;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true);
	set_process(true);
	
	snake.connect("moved", self, "checkSnakeCollision");
	snake.connect("collision", self, "gameOver");
	spawnApple(null);
	
	add_child(turnTimer);
	turnTimer.set_wait_time(1);
	turnTimer.connect("timeout", self, "onNextTurn");
	turnTimer.start();

func _input(event):
	if (event.is_action("MoveUp")):
		snake.currentDirection = snake.Directions.NORTH;
	
	if (event.is_action("MoveRight")):
		snake.currentDirection = snake.Directions.EAST;
	
	if (event.is_action("MoveDown")):
		snake.currentDirection = snake.Directions.SOUTH;
	
	if (event.is_action("MoveLeft")):
		snake.currentDirection = snake.Directions.WEST;
	
	
func spawnApple(pos):
	if (!pos):
		pos = tileMap.getRandomPosition();
	
	print("Spawn apple: ", pos);
	
	currentApple = AppleScene.instance();
	currentApple.set_pos(pos);
	add_child(currentApple);
	currentApple.connect("exit_tree", self, "onAppleExit");
	
func onAppleExit():
	print("Apple exited");
	spawnApple(null);

func onNextTurn():
	snake.nextTurn();

func checkSnakeCollision():
	if (!currentApple):
		return;

	var snakePos = tileMap.world_to_map(snake.get_pos());
	var applePos = tileMap.world_to_map(currentApple.get_pos());
	
	if (snakePos == applePos):
		remove_child(currentApple);
		snake.addBody();
	

func gameOver():
	turnTimer.stop();
	remove_child(snake);
	print("Game over!");
	get_tree().change_scene("res://GameOver.tscn");