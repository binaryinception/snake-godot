extends TileMap

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func getRandomPosition():
	var size = self.get_used_rect()
	print("Map size", size);
	
	randomize();
	var newPos;
	
	for i in range(0, 10):
		newPos = Vector2(rand_range(size.pos.x, size.end.x),  rand_range(size.pos.y, size.end.y));
		print("Tries: ", i);
		
		if (!isTileBlocked(newPos)):
			break;
	
	
	return map_to_world(newPos);

func isTileBlocked(pos):
	var tile = get_cellv(pos);
	
	print("Collide: ", tile);
	
	return tile != 1; # 1 is the floor, now.